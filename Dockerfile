FROM arm32v7/python:slim-bullseye

COPY . /app

WORKDIR /app

RUN pip install pyserial paho-mqtt requests pyyaml

CMD ["python", "ControllarServer.py"]